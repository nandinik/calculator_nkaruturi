package com.cornez.calculator;
public class SimpleExpression {
    private Double mOperand1;
    private Double mOperand2;
    private String mOperator;
    private Double mValue;

    public SimpleExpression() {
        mOperand1 = 0.0;
        mOperand2 = 0.0;
        mOperator = "+";
        mValue = 0.0;
    }

    public void setOperand1(Double v) {
        mOperand1 = v;
    }

    public Double getOperand1() {
        return mOperand1;
    }

    public void setOperand2(Double v) {
        mOperand2 = v;
    }

    public Double getOperand2() {
        return mOperand2;
    }

    public void setOperator(String s) {
        mOperator = s;
    }

    public String getOperator() {
        return mOperator;
    }

    public Double getValue() {
        computeValue();
        return mValue;
    }

    /*
     * Clears the operands within an expression
     */
    public void clearOperands() {
        mOperand1 = 0.0;
        mOperand2 = 0.0;
    }

    /*
     * Computes the integer value of the expression.
     */
    private void computeValue() {
        mValue = 0.0;

        if (mOperator.contentEquals("+"))
            mValue = mOperand1 + mOperand2;
        else if (mOperator.contentEquals("-"))
            mValue = mOperand1 - mOperand2;
        else if (mOperator.contentEquals("x"))
            mValue = mOperand1 * mOperand2;
        else if (mOperator.contentEquals("/") && mOperand2 != 0) {

            mValue = mOperand1 / mOperand2;
        }
        else if(mOperator.contentEquals("/") && mOperand2 == 0)
        {
            mValue=0.0;
        }
        else  if (mOperator.contentEquals("%")){
            if(mOperand2 !=0.0)
            {
                mValue = (mOperand1/100.0) * mOperand2;
            }
            else
                mValue = mOperand1 /100;
        }
    }

}
